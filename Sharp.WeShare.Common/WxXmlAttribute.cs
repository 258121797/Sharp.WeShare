﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Common
{
    /// <summary>
    /// XML实体自定义特性
    /// </summary>
    public class WxXmlAttribute : Attribute
    {
        /// <summary>
        /// 是否有子属性
        /// </summary>
        public bool HasChild = false;
        /// <summary>
        /// 是否是列表
        /// </summary>
        public bool IsList = false;

    }
}
