﻿using Sharp.WeShare.MP;
using Sharp.WeShare.MP.EnumKey;
using Sharp.WeShare.MP.Menu;
using Sharp.WeShare.MP.Menu.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MpTest
{
    public partial class WxMenu : System.Web.UI.Page
    {
        string accessToken = AccessToken.GetValue(WxConfig.APPID, WxConfig.APPSECRET);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var res = MenuApi.Create(accessToken, new List<ButtonInfo> {
                 new ButtonInfo
                 {
                      name="第一个菜单",
                      sub_button= new List<ButtonInfo>
                      {
                          new ButtonInfo
                          {
                               name="拍照发图",
                                type = MenuType.pic_sysphoto,
                                 key="sysphoto"
                          },
                           new ButtonInfo
                           {
                               name="发图",
                                type = MenuType.pic_photo_or_album,
                                key="or_album"
                           },
                           new ButtonInfo
                           {
                               name="发图2",
                                type = MenuType.pic_weixin,
                                key="weixin"
                           }
                      }
                 },
                  new ButtonInfo
                 {
                      name="第二个菜单",
                      type = MenuType.view,
                      url="http://www.baidu.com"
                 },
                  new ButtonInfo
                  {
                      name="第三个",
                      sub_button=new List<ButtonInfo>
                      {
                           new ButtonInfo
                 {
                      name="s第一个菜单",
                      type = MenuType.click,
                      key="sfirst"
                 },
                  new ButtonInfo
                 {
                      name="s第二个菜单",
                      type = MenuType.view,
                      url="http://www.baidu.com/s"
                 },
                  new ButtonInfo
                  {
                      name="扫码",
                      type=MenuType.scancode_push,
                       key="saoma",
                  },
                  new ButtonInfo
                  {
                      name="扫码等待",
                      type=MenuType.scancode_waitmsg,
                       key="saoma",
                  }
                      }
                  }
            });
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var res = MenuApi.Get(accessToken);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            var res = MenuApi.AddConditional(accessToken, new List<ButtonInfo> {
                 new ButtonInfo
                 {
                      name="g第一个菜单",
                      sub_button= new List<ButtonInfo>
                      {
                          new ButtonInfo
                          {
                               name="拍照发图",
                                type = MenuType.pic_sysphoto,
                                 key="sysphoto"
                          },
                           new ButtonInfo
                           {
                               name="发图",
                                type = MenuType.pic_photo_or_album,
                                key="or_album"
                           },
                           new ButtonInfo
                           {
                               name="发图2",
                                type = MenuType.pic_weixin,
                                key="weixin"
                           }
                      }
                 },
                  new ButtonInfo
                 {
                      name="g第二个菜单",
                      type = MenuType.view,
                      url="http://www.baidu.com"
                 },
                  new ButtonInfo
                  {
                      name="g第三个",
                      sub_button=new List<ButtonInfo>
                      {
                           new ButtonInfo
                 {
                      name="s第一个菜单",
                      type = MenuType.click,
                      key="sfirst"
                 },
                  new ButtonInfo
                 {
                      name="s第二个菜单",
                      type = MenuType.view,
                      url="http://www.baidu.com/s"
                 },
                  new ButtonInfo
                  {
                      name="扫码",
                      type=MenuType.scancode_push,
                       key="saoma",
                  },
                  new ButtonInfo
                  {
                      name="扫码等待",
                      type=MenuType.scancode_waitmsg,
                       key="saoma",
                  }
                      }
                  }
            },new Matchrule {client_platform_type = 2});
            //408158376
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            var res = MenuApi.DeleteConditional(accessToken, "408158376");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            var res = MenuApi.TryMatch(accessToken, "z18702711053");
        }
    }
}