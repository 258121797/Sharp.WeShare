﻿using Sharp.WeShare.MP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MpTest
{
    public partial class getip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var accesstoken = AccessToken.GetValue(WxConfig.APPID,WxConfig.APPSECRET);
            var ip = CallBackIp.GetList(accesstoken);
        }
    }
}