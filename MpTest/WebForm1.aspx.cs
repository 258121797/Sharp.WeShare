﻿using Sharp.WeShare.Common;
using Sharp.WeShare.Pay;
using System;

namespace MpTest
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //var aa =     PayApi.UnifiedOrder(new UnifiedReq
            //    {
            //        appid = WxConfig.APPID,
            //        attach = "123",
            //        body="商品",
            //        mch_id= WxConfig.MCHID,
            //        notify_url="http://www.baidu.com",
            //        out_trade_no=Utils.GetTimeStamp().ToString(),
            //        product_id="1",
            //        trade_type=TradeType.NATIVE, 
            //        total_fee=1
            //    }, WxConfig.PAYKEY);
            // }
            var red = PayApi.SendRedPack(new RedPackReq
            {
                act_name = "情人节快乐",
                total_amount = 100,
                client_ip = "192.168.0.1",

                mch_billno = Utils.GetTimeStamp().ToString(),
                mch_id = WxConfig.MCHID,
                nonce_str = Utils.GetTimeStamp().ToString(),
                re_openid = "oR19CsyUvziSlctqDmEoMwwph4Jk",
                remark = "猜越多得越多，快来抢！",
                send_name = "微企卡科技",
                total_num = 1,
                wishing = "感谢您参加猜灯谜活动，祝您元宵节快乐！",
                wxappid = WxConfig.APPID,
            }, WxConfig.PAYKEY, Request.MapPath("/apiclient_cert.p12"), WxConfig.MCHID);
        }
    }
}