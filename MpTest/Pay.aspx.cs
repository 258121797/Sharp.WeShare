﻿using Sharp.WeShare.Common;
using Sharp.WeShare.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MpTest
{
    public partial class Pay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var req = new CouponReq
            {
                appid = WxConfig.APPID,
                coupon_stock_id = "814345",
                mch_id = WxConfig.MCHID,
                partner_trade_no = Utils.GetTimeStamp().ToString(),
                openid = "oR19CsyUvziSlctqDmEoMwwph4Jk"
            };
            var a = PayApi.SendCoupon(req, WxConfig.PAYKEY,"","");
        }

        protected void Unnamed_Click1(object sender, EventArgs e)
        {
            var req = new ScanCodeTwoParam
            {
                appid = WxConfig.APPID,
                body = "冷饮",
                mch_id = WxConfig.MCHID,
                notify_url = "http://www.vqicard.com",
                out_trade_no = Utils.GetTimeStamp().ToString(),
                product_id = "1",
                spbill_create_ip = Utils.GetIp(),
                total_fee = 300
            };
            var res = PayApi.ScanCodePayTwo(req, WxConfig.PAYKEY);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var req = new CouponStockReq
            {
                appid = WxConfig.APPID,
                mch_id = WxConfig.MCHID,
                coupon_stock_id = "814345"
            };
            var res = PayApi.QueryCouponStock(req, WxConfig.PAYKEY);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var req = new CouponInfoReq
            {
                appid = WxConfig.APPID,
                mch_id = WxConfig.MCHID,
                coupon_id = "922741723",
                stock_id = "814345",
                openid= "oR19CsyUvziSlctqDmEoMwwph4Jk"
            };
            var res = PayApi.QueryCouponInfo(req, WxConfig.PAYKEY);
        }
    }
}