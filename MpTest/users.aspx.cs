﻿using Sharp.WeShare.MP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sharp.WeShare.MP.UserManager;
namespace MpTest
{
    public partial class users : System.Web.UI.Page
    {
        string at = AccessToken.GetValue(WxConfig.APPID,WxConfig.APPSECRET);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           var tag =  Sharp.WeShare.MP.UserManager.UserApi.CreateTag(at, "zssw2");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var tags = Sharp.WeShare.MP.UserManager.UserApi.GetTags(at);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            var res = Sharp.WeShare.MP.UserManager.UserApi.UpdateTag(at, 110, "ddnn");
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            var res = Sharp.WeShare.MP.UserManager.UserApi.DeleteTag(at, 110);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            var res = Sharp.WeShare.MP.UserManager.UserApi.GetTagUsers(at, 0);
        }
    }
}