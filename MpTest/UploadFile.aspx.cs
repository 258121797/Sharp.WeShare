﻿using Sharp.WeShare.Common;
using Sharp.WeShare.MP;
using Sharp.WeShare.MP.EnumKey;
using Sharp.WeShare.MP.MaterialLib;
using Sharp.WeShare.MP.MaterialLib.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace MpTest
{
    public partial class UploadFile : System.Web.UI.Page
    {
        string at = AccessToken.GetValue(WxConfig.APPID,WxConfig.APPSECRET);
        protected void Page_Load(object sender, EventArgs e)
        {
            //using (var ms = new MemoryStream())
            //{
            //    var ss = Material.GetTempFile(at, "4pVC99XlwEWL8CHbvjdNc4BE83dvI3_Pd0leXo0Cnsy_5KlSCIBEOxK2Dld3bf_I", ms);
            //    File.WriteAllBytes(MapPath("111.jpg"), ms.GetBuffer());
            //}
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var media = MaterialApi.AddTemp(at, MapPath("/11.jpg"), MaterialType.image);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var media = MaterialApi.Add(at, MapPath("1.mp4"), MaterialType.video, "hhh", "这是一条测试视频");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            var url = MaterialApi.UploadNewImgUrl(at, MapPath("/11.jpg"));
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            var arts = new List<Article> {
                new Article
                {
                      content="-",
                       title="123123",
                       thumb_media_id="Qh7CfSo5w0gSZGoSjwxBvxcWdr7oVWQ1i1GSK1NFBgG35D_7bEjzUjuMPoSLWKM4"
                }
            };
          var res=  MaterialApi.AddNews(at, new Articles { articles= arts });
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            var arts = MaterialApi.GetNews(at, "T2He_9Hf1lN5IsJC_BNAER21lw6JcuNpfI-dystQZS4");
            //更新多图文
            //arts.news_item[0].content_source_url = "http://www.baidu.com";
            //var upres = Material.UpdateNewItem(at, "T2He_9Hf1lN5IsJC_BNAER21lw6JcuNpfI-dystQZS4",0,arts.news_item[0]);
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            var video = MaterialApi.GetVideo(at, "T2He_9Hf1lN5IsJC_BNAEXJDFynQEZdKEf80hN8KKcI");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            using (var ms = new FileStreamInfo())
            {
                var ss = MaterialApi.Get(at, "T2He_9Hf1lN5IsJC_BNAEQK__mMzyIu1FR5SfIb-OCs", ms);
                if (ss.errcode == 0)
                {
                    File.WriteAllBytes(MapPath("/"+ms.filename), ms.GetBuffer());
                }
            }
        }
    }
}