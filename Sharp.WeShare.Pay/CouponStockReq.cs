﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Pay
{
    public class CouponStockReq:BasePay
    {
        /// <summary>
        /// 代金券批次id
        /// </summary>
        public string coupon_stock_id { get; set; }
        /// <summary>
        /// 操作员.默认为商户号
        /// </summary>
        public string op_user_id { get; set; }
        /// <summary>
        /// 设备号
        /// </summary>
        public string device_info { get; set; }
        /// <summary>
        /// 协议版本. 默认1.0 
        /// </summary>
        public string version { get; set; } = "1.0";
        /// <summary>
        ///协议类型 XML【目前仅支持默认XML】 
        /// </summary>
        public string type { get; set; } = "XML";

    }
}
