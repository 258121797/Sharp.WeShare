﻿using Sharp.WeShare.Common;

namespace Sharp.WeShare.Pay
{
    /// <summary>
    ///微信支付基类
    /// </summary>
    public abstract class BasePay
    {
        public string appid { get; set; }
        /// <summary>
        /// 商户号
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 随机字符串。
        /// </summary>
        public string nonce_str { get; set; } = Utils.GetTimeStamp().ToString();
    }
}