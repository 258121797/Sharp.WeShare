﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Pay
{
    /// <summary>
    /// 代金券信息
    /// </summary>
    public class CouponInfoRes : BasePayRes
    {
        /// <summary>
        /// 设备号
        /// </summary>
        public string device_info { get; set; }
        /// <summary>
        /// 微信支付分配的子商户号，受理模式下必填
        /// </summary>
        public string sub_mch_id { get; set; }
        /// <summary>
        /// 代金券批次ID
        /// </summary>
        public string coupon_stock_id { get; set; }
        /// <summary>
        /// 代金券名称
        /// </summary>
        public string coupon_name { get; set; }
        /// <summary>
        /// 批次类型；1-批量型，2-触发型
        /// </summary>
        public string coupon_stock_type { get; set; }
        /// <summary>
        /// 代金券id
        /// </summary>
        public string coupon_id { get; set; }
        /// <summary>
        /// 代金券面额.单位是分
        /// </summary>
        public int coupon_value { get; set; }
        /// <summary>
        /// 代金券使用最低限额, 单位是分
        /// </summary>
        public string coupon_mininumn { get; set; }
        /// <summary>
        /// 代金券类型.1-代金券无门槛，2-代金券有门槛互斥，3-代金券有门槛叠加，
        /// </summary>
        public string coupon_type { get; set; }
        /// <summary>
        /// 代金券使用门槛
        /// </summary>
        public int coupon_mininum { get; set; }
        /// <summary>
        /// 代金券数量
        /// </summary>
        public int coupon_total { get; set; }
        /// <summary>
        /// 代金券状态：2-已激活，4-已锁定，8-已实扣
        /// </summary>
        public string coupon_state { get; set; }
        /// <summary>
        /// 代金券描述
        /// </summary>
        public string coupon_desc { get; set; }
        /// <summary>
        /// 代金券实际使用金额
        /// </summary>
        public int coupon_use_value { get; set; }
        /// <summary>
        /// 代金券剩余金额：部分使用情况下，可能会存在券剩余金额
        /// </summary>
        public int coupon_remain_value { get; set; }
        /// <summary>
        /// 生效开始时间.格式为yyyyMMddhhmmss
        /// </summary>
        public string begin_time { get; set; }
        /// <summary>
        /// 生效结束时间
        /// </summary>
        public string end_time { get; set; }
        /// <summary>
        /// 发放时间
        /// </summary>
        public string send_time { get; set; }
        /// <summary>
        /// 使用时间
        /// </summary>
        public string use_time { get; set; }
        /// <summary>
        /// 使用单号
        /// </summary>
        public string trade_no { get; set; }
        /// <summary>
        /// 消耗方商户id
        /// </summary>
        public string consumer_mch_id { get; set; }
        /// <summary>
        /// 消耗方商户名称
        /// </summary>
        public string consumer_mch_name { get; set; }
        /// <summary>
        /// 消耗方商户appid
        /// </summary>
        public string consumer_mch_appid { get; set; }
        /// <summary>
        /// 代金券发放来源:JIFA-即发即用 NORMAL-普通发劵 FULL_SEND-满送活动送劵 SCAN_CODE-扫码领劵 OZ-刮奖领劵 AJUST-对账调账
        /// </summary>
        public string send_source { get; set; }
        /// <summary>
        /// 该代金券是否允许部分使用标识：1-表示支持部分使用 
        /// </summary>
        public bool is_partial_use { get; set; }

    }
}
