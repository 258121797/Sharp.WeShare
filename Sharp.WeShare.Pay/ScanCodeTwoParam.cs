﻿namespace Sharp.WeShare.Pay
{
    /// <summary>
    /// 扫码支付模式二的参数集合
    /// </summary>
    public class ScanCodeTwoParam : BasePay
    {
        /// <summary>
        /// 微信支付分配的终端设备号
        /// </summary>
        public string device_info { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 商品描述
        /// </summary>
        public string body { get; set; }
        /// <summary>
        /// 商品详情
        /// 商品详细列表，使用Json格式，传输签名前请务必使用CDATA标签将JSON文本串保护起来。goods_detail 服务商必填[]：
        /// goods_id String 必填 32 商品的编号
        /// wxpay_goods_id String 可选 32 微信支付定义的统一商品编号
        /// goods_name String 必填 256 商品名称
        /// quantity Int 必填 商品数量
        /// price Int 必填 商品单价，单位为分
        /// goods_category String 可选 32 商品类目ID
        /// body String 可选 1000 商品描述信息
        /// </summary>
        public string detail { get; set; }
        
        /// <summary>
        /// 附加数据，原样返回
        /// </summary>
        public string attach { get; set; }
        /// <summary>
        /// 商户系统内部的订单号、32个字符内、可包含字母、确保在商户系统唯一
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 货币类型 
        /// </summary>
        public string fee_type { get; set; } = "CNY";
        /// <summary>
        /// 订单总金额，单位为分，不能带小数点
        /// </summary>
        public int total_fee { get; set; }
        /// <summary>
        /// 终端IP
        /// </summary>
        public string spbill_create_ip { get; set; }
        /// <summary>
        /// 交易起始时间
        /// </summary>
        public string time_start { get; set; }
        /// <summary>
        /// 交易结束时间
        /// </summary>
        public string time_expire { get; set; }
        /// <summary>
        /// 商品标记 
        /// </summary>
        public string goods_tag { get; set; }
        /// <summary>
        /// 接收微信支付成功通知
        /// </summary>
        public string notify_url { get; set; }
        /// <summary>
        /// 只在 trade_type 为 NATIVE 时需要填写。此ID为二维码中包含的商品ID，商户自行维护。
        /// </summary>
        public string product_id { get; set; }
        /// <summary>
        /// no_credit--指定不能使用信用卡支付
        /// </summary>
        public string limit_pay { get; set; } = "no_credit";
    }
}
