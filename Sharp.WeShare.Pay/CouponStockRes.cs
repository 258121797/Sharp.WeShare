﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Pay
{
    public class CouponStockRes : BasePayRes
    {
        /// <summary>
        /// 设备号
        /// </summary>
        public string device_info { get; set; }
        /// <summary>
        /// 代金券批次ID
        /// </summary>
        public string coupon_stock_id { get; set; }
        /// <summary>
        /// 代金券名称
        /// </summary>
        public string coupon_name { get; set; }
        /// <summary>
        /// 代金券面额.单位是分
        /// </summary>
        public int coupon_value { get; set; }
        /// <summary>
        /// 代金券使用最低限额, 单位是分
        /// </summary>
        public string coupon_mininumn { get; set; }
        /// <summary>
        /// 代金券类型.1-代金券无门槛，2-代金券有门槛互斥，3-代金券有门槛叠加，
        /// </summary>
        public int coupon_type { get; set; }
        /// <summary>
        /// 代金券批次状态. 1-未激活；2-审批中；4-已激活；8-已作废；16-中止发放； 
        /// </summary>
        public int coupon_stock_status { get; set; }
        /// <summary>
        /// 代金券数量
        /// </summary>
        public int coupon_total { get; set; }
        /// <summary>
        /// 代金券最大领取数量.代金券每个人最多能领取的数量, 如果为0，则表示没有限制
        /// </summary>
        public int max_quota { get; set; }
        /// <summary>
        /// 代金券锁定数量
        /// </summary>
        public int locked_num { get; set; }
        /// <summary>
        /// 代金券已使用数量
        /// </summary>
        public int used_num { get; set; }
        /// <summary>
        /// 代金券已经发送的数量
        /// </summary>
        public int is_send_num { get; set; }
        /// <summary>
        /// 生效开始时间.格式为yyyyMMddhhmmss
        /// </summary>
        public string begin_time { get; set; }
        /// <summary>
        /// 生效结束时间
        /// </summary>
        public string end_time { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string create_time { get; set; }
        /// <summary>
        /// 代金券预算额度
        /// </summary>
        public int coupon_budget { get; set; }
    }
}
