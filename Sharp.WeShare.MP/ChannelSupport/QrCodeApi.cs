﻿using Sharp.WeShare.Common;
using Sharp.WeShare.MP.ChannelSupport.Entity;

namespace Sharp.WeShare.MP.ChannelSupport
{
    /// <summary>
    /// 带参数二维码操作类
    /// </summary>
    public class QrCodeApi
    {
        /// <summary>
        /// 生成临时二维码
        /// </summary>
        /// <param name="expireSeconds">单位（秒）过期时间，30天</param>
        /// <param name="sceneId">场景值ID，临时二维码时为32位非0整型，即最大值4294967295，最小值-4294967295，不含0</param>
        /// <param name="accessToken">accessToken</param>
        /// <returns>ticket实体，ticket可以换取二维码，也可以根据URL自行生成。</returns>
        public static QrTicket CreateTemp(string accessToken, long sceneId, int expireSeconds)
        {
            var json = new
            {
                expire_seconds = expireSeconds,
                action_name = "QR_SCENE",
                action_info = new { scene = new { scene_id = sceneId } }
            };
            return Create(accessToken, json);
        }
        /// <summary>
        /// 创建场景值为数字的永久二维码
        /// </summary>
        /// <param name="sceneId">场景值，有效范围1-100000</param>
        /// <param name="accessToken">accessToken</param>
        /// <returns>ticket实体，ticket可以换取二维码，也可以根据URL自行生成。</returns>
        public static QrTicket CreateByID(string accessToken, int sceneId)
        {
            var json = new
            {
                action_name = "QR_LIMIT_SCENE",
                action_info = new { scene = new { scene_id = sceneId } }
            };
            return Create(accessToken, json);
        }

        /// <summary>
        /// 创建场景值为字符串的永久二维码
        /// </summary>
        /// <param name="sceneId">场景值，有效范围1-100000</param>
        /// <param name="accessToken">accessToken</param>
        /// <returns>ticket实体，ticket可以换取二维码，也可以根据URL自行生成。</returns>
        public static QrTicket CreateByStr(string accessToken, string sceneStr)
        {
            var json = new
            {
                action_name = "QR_LIMIT_STR_SCENE",
                action_info = new { scene = new { scene_str = sceneStr } }
            };
            return Create(accessToken, json);
        }
        /// <summary>
        /// 根据ticket获取二维码文件的url
        /// </summary>
        /// <param name="ticket"></param>
        /// <returns></returns>
        public static string GetQrByTicket(string ticket)
        {
            return string.Format("https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}", ticket);
        }


        private static QrTicket Create(string accessToken, object obj)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}", accessToken);
            return Utils.PostResult<QrTicket>(obj, url);
        }

        /// <summary>
        /// 将一条长链接转成短链接
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <param name="longurl">长链接</param>
        /// <returns>包含短连接和错误代码的实体</returns>
        public static ShortUrl LongUrlToShortUrl(string accessToken, string longurl)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/shorturl?access_token={0}", accessToken);
            var json = new { action = "long2short", long_url = longurl };
            return Utils.PostResult<ShortUrl>(json, url);
        }
    }
}