﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Menu.Entity
{
    public class MenuInfo
    {
        /// <summary>
        /// 普通菜单
        /// </summary>
        public Buttons menu { get; set; }
        /// <summary>
        /// 个性化菜单
        /// </summary>
        public Buttons conditionalmenu { get; set; }
    }
}
