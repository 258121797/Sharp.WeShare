﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Menu.Entity
{
    public class Buttons:BaseRes
    {
        public List<ButtonInfo> button { get; set; }
        /// <summary>
        /// 菜单ID 个性化菜单有效
        /// </summary>
        public string menuid { get; set; }
        /// <summary>
        /// 匹配规则，个性化菜单有效
        /// </summary>
        public Matchrule matchrule { get; set; }
    }
}
