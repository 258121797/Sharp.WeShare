﻿using Newtonsoft.Json.Linq;
using Sharp.WeShare.Common;
using Sharp.WeShare.MP.Menu.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Menu
{
    public class MenuApi
    {
        /// <summary>
        /// 创建普通菜单
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="btns"></param>
        /// <returns></returns>
        public static BaseRes Create(string accessToken,List<ButtonInfo> btns)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}",accessToken);
            var data = new { button = btns };
            return Utils.PostResult<BaseRes>(data, url);
        }
        /// <summary>
        /// 添加个性化菜单
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="btns"></param>
        /// <param name="rule"></param>
        /// <returns></returns>
        public static MenuId AddConditional(string accessToken, List<ButtonInfo> btns,Matchrule rule)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token={0}", accessToken);
            var data = new { button = btns, matchrule = rule };
            return Utils.PostResult<MenuId>(data, url);
        }
        /// <summary>
        /// 删除个性化菜单
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public static BaseRes DeleteConditional(string accessToken,string menuId)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token={0}",accessToken);
            var data = new { menuid = menuId };
            return Utils.PostResult<BaseRes>(data, url);
        }
        /// <summary>
        /// 测试个性化菜单匹配结果
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static MenuInfo TryMatch(string accessToken, string userId)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token={0}", accessToken);
            var data = new { user_id = userId };
            return Utils.PostResult<MenuInfo>(data, url);
        }
        /// <summary>
        /// 自定义菜单查询接口
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static MenuInfo Get(string accessToken)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/menu/get?access_token={0}", accessToken);
            return Utils.GetResult<MenuInfo>(url);
        }

    }
}
