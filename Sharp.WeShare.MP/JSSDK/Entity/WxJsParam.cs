﻿namespace Sharp.WeShare.MP.JSSDK.Entity
{
    /// <summary>
    ///调用jssdk的参数
    /// </summary>
    public class WxJsParam
    {
        /// <summary>
        /// 时间戳
        /// </summary>
        public int timestamp { get; set; }
        /// <summary>
        /// 随机字符串
        /// </summary>
        public string nonceStr { get; set; }
        /// <summary>
        /// 当前页面的url
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string signature { get; set; }
        /// <summary>
        /// 是否开启调试模式
        /// </summary>
        public bool debug { get; set; }
        /// <summary>
        /// 公众号ID
        /// </summary>
        public string appId { get; set; }
    }
}