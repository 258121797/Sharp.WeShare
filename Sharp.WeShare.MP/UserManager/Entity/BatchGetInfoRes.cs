﻿using System.Collections.Generic;

namespace Sharp.WeShare.MP.UserManager.Entity
{
    /// <summary>
    /// 批量获取用户信息时，返回的实体
    /// </summary>
    public class BatchGetInfoRes:BaseRes
    {
        /// <summary>
        /// 用户信息列表
        /// </summary>
        public List<UserInfo> user_info_list { get; set; }
    }
}