﻿using System.Collections.Generic;

namespace Sharp.WeShare.MP.UserManager.Entity
{
    public class TagRes:BaseRes
    {
        /// <summary>
        /// 标签名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 标签id
        /// </summary>
        public int id { get; set; }
    }
    /// <summary>
    /// 获取标签列表实体类
    /// </summary>
    public class TagsRes:BaseRes
    {
        public List<Tag> tags { get; set; }
    }
    public class Tag
    {
        /// <summary>
        /// 标签名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 标签id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 此标签下粉丝数
        /// </summary>
        public int count { get; set; }
    }
}
