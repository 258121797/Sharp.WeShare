﻿using System;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.MP.UserManager.Entity
{
    public class UserInfo:BaseRes
    {
        /// <summary>
        /// 是否关注，仅适用获取用户基本信息接口有效。以snsapi_userinfo方式进行网页授权接口调用的是无效的。
        /// </summary>
        public int subscribe { get; set; }
        public string openid { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string nickname { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int sex { get; set; }
        /// <summary>
        /// 语言
        /// </summary>
        public string language { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string city { get; set; }
        /// <summary>
        /// 广东
        /// </summary>
        public string province { get; set; }
        /// <summary>
        /// 中国
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// 图像
        /// </summary>
        public string headimgurl { get; set; }
        /// <summary>
        /// 关注时间，时间戳
        /// </summary>
        public int subscribe_time { get; set; }
        /// <summary>
        /// 关注时间。  
        /// </summary>
        public DateTime SubscribeTime
        {
            get { return Utils.UnixTimeToTime(subscribe_time.ToString()); }

        }
        /// <summary>
        /// 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
        /// </summary>
        public string unionid { get; set; }
        /// <summary>
        /// 用户备注
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 用户所在的分组ID（兼容旧的用户分组接口）
        /// </summary>
        public int groupid { get; set; }
        /// <summary>
        /// 用户被打上的标签ID列表
        /// </summary>
        public int[] tagid_list { get; set; }
    }
}