﻿using System.Web;
using Sharp.WeShare.Common;
using Sharp.WeShare.MP.EnumKey;
using Sharp.WeShare.MP.UserManager.Entity;

namespace Sharp.WeShare.MP.UserManager
{
    /// <summary>
    /// 微信网页授权
    /// </summary>
    public class OauthApi
    {
        /// <summary>
        /// 获取授权的url，默认使用snsapi_base方式
        /// </summary>
        /// <param name="appid">公众号的唯一标识</param>
        /// <param name="redirect_uri">授权后重定向的回调链接地址，请使用urlencode对链接进行处理</param>
        /// <param name="state">重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节</param>
        /// <param name="authType">应用授权作用域</param>
        /// <returns></returns>
        public static string GetAuthUrl(string appid, string redirect_uri, string state, AuthType authType = AuthType.snsapi_base)
        {
            var url =
            "https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope={2}&state={3}#wechat_redirect";
            return string.Format(url, appid, HttpUtility.UrlEncode(redirect_uri), authType, state);
        }
        /// <summary>
        /// 检查AuthToken是否有效
        /// </summary>
        /// <param name="authtoken"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        public static BaseRes CheckAuthToken(string authtoken, string openid)
        {
            var url =
            string.Format("https://api.weixin.qq.com/sns/auth?access_token={0}&openid={1}", authtoken, openid);
            return Utils.GetResult<BaseRes>(url);
        }
        /// <summary>
        /// 刷新网页授权access_token.
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="refresh_token"></param>
        /// <returns></returns>
        public static OAuthToken GetRefreshToken(string appid, string refresh_token)
        {
            var url =
            string.Format("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={0}&grant_type=refresh_token&refresh_token={1}", appid, refresh_token);
            return Utils.GetResult<OAuthToken>(url);
        }
        /// <summary>
        /// 网页授权获取用户信息，仅网页授权作用域为snsapi_userinfo时有效
        /// </summary>
        /// <param name="authtoken"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        public static UserInfo GetUserInfo(string authtoken, string openid,string lang="zh_CN")
        {
            var url =
            string.Format("https://api.weixin.qq.com/sns/userinfo?access_token={0}&openid={1}&lang={2}", authtoken, openid,lang);
            return Utils.GetResult<UserInfo>(url);
        }
        /// <summary>
        /// 通过code换取网页授权access_token
        /// </summary>
        /// <param name="appid">公众号的唯一标识</param>
        /// <param name="secret">公众号的appsecret</param>
        /// <param name="code">填写第一步获取的code参数</param>
        /// <returns></returns>
        public static OAuthToken GetAuthToken(string appid, string secret, string code)
        {
            var url =
                string.Format(
                    "https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type=authorization_code"
                    , appid, secret, code);
            return Utils.GetResult<OAuthToken>(url);
        }

    }
}