﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.CustomService.Entity
{
   public class Video
    {
        public string title { get; set; }
        public string description { get; set; }
        public string thumb_media_id { get; set; }
        public string media_id { get; set; }
    }
}
