﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.CustomService.Entity
{
    /// <summary>
    /// 消息记录实体
    /// </summary>
    public class MsgRecord
    {
        /// <summary>
        /// 用户标识
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 操作码，2002（客服发送信息），2003（客服接收消息）
        /// </summary>
        public string opercode { get; set; }
        /// <summary>
        /// 聊天记录
        /// </summary>
        public string text { get; set; }
        /// <summary>
        /// 操作时间，unix时间戳
        /// </summary>
        public string time { get; set; }
        /// <summary>
        /// 完整客服帐号，格式为：帐号前缀@公众号微信号
        /// </summary>
        public string worker { get; set; }
    }
}
