﻿using System;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.MP.CustomService.Entity
{
    /// <summary>
    /// 获取客户会话状态
    /// </summary>
    public class KfStatusInfo:BaseRes
    {
        /// <summary>
        /// 会话接入的时间
        /// </summary>
        public string createtime { get; set; }
        public string kf_account { get; set; }

        public DateTime JoinTime
        {
            get { return Utils.UnixTimeToTime(createtime); }
        }
    }
}