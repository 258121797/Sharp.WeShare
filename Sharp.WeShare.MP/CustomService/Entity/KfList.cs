﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.CustomService.Entity
{
    public class KfList : BaseRes
    {
        public List<KfInfo> kf_list { get; set; }
    }
    public class KfInfo
    {
        /// <summary>
        /// 完整客服账号，格式为：账号前缀@公众号微信号
        /// </summary>
        public string kf_account { get; set; }
        /// <summary>
        /// 客服头像
        /// </summary>
        public string kf_headimgurl { get; set; }
        /// <summary>
        /// 客服工号
        /// </summary>
        public string kf_id { get; set; }
        /// <summary>
        /// 客服昵称
        /// </summary>
        public string kf_nick { get; set; }
        /// <summary>
        /// 客服微信
        /// </summary>
        public string kf_wx { get; set; }
        /// <summary>
        /// 如果客服帐号尚未绑定微信号，但是已经发起了一个绑定邀请，
        ///则此处显示绑定邀请的微信号
        /// </summary>
        public string invite_wx { get; set; }
        /// <summary>
        /// 如果客服帐号尚未绑定微信号，但是已经发起过一个绑定邀请，邀请的过期时间，为unix 时间戳
        /// </summary>
        public int invite_expire_time { get; set; }
        /// <summary>
        /// 邀请的状态，有等待确认“waiting”，被拒绝“rejected”，过期“expired”
        /// </summary>
        public InviteStatus? invite_status { get; set; }
    }
}
