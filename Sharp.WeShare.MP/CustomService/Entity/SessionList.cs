﻿using System;
using System.Collections.Generic;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.MP.CustomService.Entity
{
    public class SessionList:BaseRes
    {
        public List<session> sessionlist { get; set; }
        public class session 
        {
            /// <summary>
            /// 会话接入的时间
            /// </summary>
            public string createtime { get; set; }
            public string openid { get; set; }

            public DateTime JoinTime
            {
                get { return Utils.UnixTimeToTime(createtime); }
            }
        }
    }
}