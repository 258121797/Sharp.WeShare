﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.CustomService.Entity
{
    /// <summary>
    /// 消息记录列表
    /// </summary>
    public class MsgRecordRes:BaseRes
    {
        public List<MsgRecord> recordlist { get; set; }
        public int number { get; set; }
        public int msgid { get; set; }
    }
}
