﻿using Sharp.WeShare.Common;
using Sharp.WeShare.MP.CustomService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.CustomService
{
    public class CustomServiceApi
    {
        /// <summary>
        /// 添加或更新客服
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="account"></param>
        /// <param name="nickName"></param>
        /// <param name="password"></param>
        /// <param name="isUpdate">是否是更新， 否则是添加</param>
        /// <returns></returns>
        public static BaseRes SetAccount(string accessToken, string account, string nickName, bool isUpdate = false)
        {
            var url =
            string.Format("https://api.weixin.qq.com/customservice/kfaccount/{0}?access_token={1}", isUpdate ? "update" : "add", accessToken);
            var obj = new
            {
                kf_account = account,
                nickname = nickName
            };
            return Utils.PostResult<BaseRes>(obj, url);
        }
        /// <summary>
        /// 删除客服
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static BaseRes DeleteAccount(string accessToken, string account)
        {
            var url =
            string.Format("https://api.weixin.qq.com/customservice/kfaccount/del?access_token={0}&kf_account={1}", accessToken, account);
            return Utils.GetResult<BaseRes>(url);
        }
        /// <summary>
        /// 邀请绑定客服帐号.此接口发起一个绑定邀请到客服人员微信号，客服人员需要在微信客户端上用该微信号确认后帐号才可用。尚未绑定微信号的帐号可以进行绑定邀请操作，邀请未失效时不能对该帐号进行再次绑定微信号邀请。
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="account">要绑定的客服账号</param>
        /// <param name="wxid">用户的微信号</param>
        /// <returns></returns>
        public static BaseRes InviteWorker(string accessToken, string account, string wxid)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfaccount/inviteworker?access_token={0}", accessToken);
            var data = new { kf_account = account, invite_wx = wxid };
            return Utils.PostResult<BaseRes>(data, url);
        }
        /// <summary>
        /// 设置客服头像
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="filepath"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static BaseRes UploadHeadImg(string accessToken, string filepath, string account)
        {
            var url = string.Format("http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token={0}&kf_account={1}", accessToken, account);
            var formlist = new List<FormEntity> { new FormEntity { IsFile = true, Name = "media", Value = filepath } };
            return Utils.UploadResult<BaseRes>(url, formlist);
        }
        /// <summary>
        /// 获取客服列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static KfList GetKfList(string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token={0}", accessToken);
            return Utils.GetResult<KfList>(url);
        }
        /// <summary>
        /// 创建会话.此接口在客服和用户之间创建一个会话，如果该客服和用户会话已存在，则直接返回0。。
        /// </summary>指定的客服帐号必须已经绑定微信号且在线
        /// <param name="accessToken"></param>
        /// <param name="openId"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static BaseRes CreateSession(string accessToken, string openId, string account)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/create?access_token={0}", accessToken);
            var data = new { kf_account = account, openid = openId };
            return Utils.PostResult<BaseRes>(data, url);
        }
        /// <summary>
        /// 关闭回话
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="openId"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static BaseRes CloseSession(string accessToken, string openId, string account)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/close?access_token={0}", accessToken);
            var data = new { kf_account = account, openid = openId };
            return Utils.PostResult<BaseRes>(data, url);
        }
        /// <summary>
        /// 获取客户会话状态
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="openId"></param>
        /// <returns></returns>
        public static KfStatusInfo GetKfStatusInfo(string accessToken, string openId)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/getsession?access_token={0}&openid={1}", accessToken, openId);
            return Utils.GetResult<KfStatusInfo>(url);
        }
        /// <summary>
        /// 获取回话列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static SessionList GetSessionList(string accessToken, string account)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/getsessionlist?access_token={0}&kf_account={1}", accessToken, account);
            return Utils.GetResult<SessionList>(url);
        }
        /// <summary>
        /// 获取未接入会话列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static WaitCaseList GetWaitCaseList(string accessToken)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/getwaitcase?access_token={0}", accessToken);
            return Utils.GetResult<WaitCaseList>(url);
        }
        /// <summary>
        /// 获取消息记录
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="msgId">消息id顺序从小到大，从1开始</param>
        /// <param name="number">每次获取条数，最多10000条</param>
        /// <returns></returns>
        public static MsgRecordRes GetMsgList(string accessToken, DateTime startTime, DateTime endTime, int msgId, int number)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/msgrecord/getmsglist?access_token={0}", accessToken);
            var data = new
            {
                starttime = Utils.ConvertDateTimeInt(startTime),
                endtime = Utils.ConvertDateTimeInt(endTime),
                msgid = msgId,
                number = number
            };
            return Utils.PostResult<MsgRecordRes>(data, url);
        }
        private static BaseRes Send(string accessToken, object data)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}", accessToken);
            return Utils.PostResult<BaseRes>(data, url);
        }
        /// <summary>
        /// 发送文本消息
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="txt"></param>
        /// <param name="openId"></param>
        /// <returns></returns>
        public static BaseRes SendText(string accessToken, string openId, string txt, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "text",
                text = new
                {
                    content = txt
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }
        public static BaseRes SendImage(string accessToken, string openId, string mediaId, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "image",
                image = new
                {
                    media_id = mediaId
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }
        public static BaseRes SendVoice(string accessToken, string openId, string mediaId, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "voice",
                voice = new
                {
                    media_id = mediaId
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }

        public static BaseRes SendMusic(string accessToken, string openId, Music music, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "music",
                music = new
                {
                    media_id = music
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }
        public static BaseRes SendVideo(string accessToken, string openId, Video video, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "video",
                video = new
                {
                    media_id = video
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }
        /// <summary>
        /// 发送图文消息（点击跳转到外链）
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="openId"></param>
        /// <param name="arts"></param>
        /// <returns></returns>
        public static BaseRes SendNews(string accessToken, string openId, List<Article> arts, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "news",
                news = new
                {
                    articles = arts
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }
        /// <summary>
        /// 发送图文消息（点击跳转到图文消息页面） 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="openId"></param>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public static BaseRes SendArts(string accessToken, string openId, string mediaId, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "mpnews",
                mpnews = new
                {
                    media_id = mediaId
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }

        public static BaseRes SendCard(string accessToken, string openId, string cardId, string kfAccount = "")
        {
            var data = new
            {
                touser = openId,
                msgtype = "wxcard",
                wxcard = new
                {
                    card_id = cardId
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(accessToken, data);
        }
    }
}
