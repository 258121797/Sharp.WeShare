﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    /// <summary>
    /// 被动回复视频消息的实体类
    /// </summary>
   public class ResponseVideo
    {
        /// <summary>
        /// 视频文件的媒体ID
        /// </summary>
        public string MediaId { get; set; }
        /// <summary>
        /// 视频消息的标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 视频消息的描述
        /// </summary>
        public string Description { get; set; }
    }

}
