﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    /// <summary>
    /// 自定义菜单跳转
    /// </summary>
    public class ViewEventMsg : EventMsg
    {
        public string EventKey { get; set; }
        /// <summary>
        /// 自定义菜单的ID
        /// </summary>
        public string MenuId { get; set; }
    }

}
