﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    /// <summary>
    /// 认证成功以及年审通知，认证过期失效推送的消息实体
    /// </summary>
    public class VerifyEventMsg:EventMsg
    {
        /// <summary>
        /// 有效期 (整形)，指的是时间戳，将于该时间戳认证过期
        /// </summary>
        public int ExpiredTime { get; set; }
        /// <summary>
        /// 失败发生时间 (整形)，时间戳，仅认证失败时有效
        /// </summary>
        public int FailTime { get; set; }
        /// <summary>
        /// 认证失败的原因，仅认证失败时有效
        /// </summary>
        public string FailReason { get; set; }
    }
}
