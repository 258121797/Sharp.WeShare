﻿using System.Collections.Generic;

namespace Sharp.WeShare.MP.Poi.Entity
{
    /// <summary>
    /// 获取门店列表返回的实体
    /// </summary>
    public class Pois:BaseRes
    {
        /// <summary>
        /// 门店列表
        /// </summary>
        public List<PoiInfo.Business> business_list { get; set; }

        /// <summary>
        /// 门店总数量
        /// </summary>
        public int total_count { get; set; }
    }
}