﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MaterialLib.Entity
{
    /// <summary>
    /// 上传素材后，响应实体
    /// </summary>
    public class MediaRes : BaseRes
    {
        public string media_id { get; set; }
    }
}
