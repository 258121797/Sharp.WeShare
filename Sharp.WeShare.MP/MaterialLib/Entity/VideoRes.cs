﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MaterialLib.Entity
{
    /// <summary>
    /// 视频素材
    /// </summary>
    public class VideoRes
    {
        public string title { get; set; }
        public string description { get; set; }
        public string down_url { get; set; }
    }
}
