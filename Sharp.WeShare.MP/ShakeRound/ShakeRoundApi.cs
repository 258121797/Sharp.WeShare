﻿using Sharp.WeShare.Common;
using Sharp.WeShare.MP.EnumKey;
using Sharp.WeShare.MP.ShakeRound.Entity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace Sharp.WeShare.MP.ShakeRound
{
    public class ShakeRoundApi
    {
        /// <summary>
        ///  申请开通摇一摇周边功能
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static BaseRes Regediter(string accessToken, RegediterParam param)
        {
            var url = string.Format("https://api.weixin.qq.com/shakearound/account/register?access_token={0}", accessToken);
            return Utils.PostResult<BaseRes>(param, url);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public static AddPageRes AddPage(string accessToken,PageInfo info)
        {
            var url = string.Format("https://api.weixin.qq.com/shakearound/page/add?access_token={0}",accessToken);
            return Utils.PostResult<AddPageRes>(info, url);
        }
        /// <summary>
        /// 上次摇周边素材
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="filePath"></param>
        /// <param name="type">Icon：摇一摇页面展示的icon图；License：申请开通摇一摇周边功能时需上传的资质文件；若不传type，则默认type=icon</param>
        /// <returns></returns>
        public static MaterialUrl UploadMaterial(string accessToken,string filePath, ShakeRoundMaterialType type=ShakeRoundMaterialType.icon)
        {
            var url = string.Format("https://api.weixin.qq.com/shakearound/material/add?access_token={0}&type={1}",accessToken,type.ToString());
            var forms = new List<FormEntity>
            {
                 new FormEntity { IsFile=false, Name="media", Value="11111.jpg" },
                 new FormEntity { IsFile=true, Name="media", Value=filePath },

            };
            return Utils.UploadResult<MaterialUrl>(url, filePath);
        }
    }
}
