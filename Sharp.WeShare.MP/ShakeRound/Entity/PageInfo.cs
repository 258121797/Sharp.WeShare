﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.ShakeRound.Entity
{
    public class PageInfo
    {
        /// <summary>
        /// 在摇一摇页面展示的主标题，不超过6个汉字或12个英文字母
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 在摇一摇页面展示的副标题，不超过7个汉字或14个英文字母
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 图标链接
        /// </summary>
        public string icon_url { get; set; }
        /// <summary>
        /// 页面的备注信息，不超过15个汉字或30个英文字母
        /// </summary>
        public string comment { get; set; }
        /// <summary>
        /// 页面的链接
        /// </summary>
        public string page_url { get; set; }
    }
}
