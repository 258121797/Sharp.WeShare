﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 封面摘要简介
    /// </summary>
    public class Abstract
    {
        /// <summary>
        /// 封面摘要简介
        /// </summary>
        public string @abstract { get; set; }
        /// <summary>
        /// 封面图片列表，仅支持填入一个封面图片链接， 需调用微信的上传图片接口，建议图片尺寸像素850*350
        /// </summary>
        public string[] icon_url_list { get; set; }
    }
}
