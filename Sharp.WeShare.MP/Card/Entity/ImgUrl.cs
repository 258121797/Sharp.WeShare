﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class ImgUrl : BaseRes
    {
        /// <summary>
        /// 商户图片url，用于创建卡券接口中填入。特别注意：该链接仅用于微信相关业务，不支持引用。
        /// </summary>
        public string url { get; set; }
    }
}
