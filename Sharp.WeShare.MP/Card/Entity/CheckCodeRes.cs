﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 导入自定义code，返回的结果。
    /// </summary>
   public class CheckCodeRes : BaseRes
    {
        /// <summary>
        /// 已经成功存入的code。
        /// </summary>
        public string[] exist_code { get; set; }
        /// <summary>
        /// 没有存入的code。
        /// </summary>
        public string[] not_exist_code { get; set; }
    }
}
