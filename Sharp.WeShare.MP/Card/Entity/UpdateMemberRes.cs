﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 更新会员信息返回的实体类
    /// </summary>
    public class UpdateMemberRes : BaseRes
    {
        /// <summary>
        /// 当前用户积分总额
        /// </summary>
        public int result_bonus { get; set; }
        /// <summary>
        /// 当前用户预存总金额        
        /// </summary>
        public int result_balance { get; set; }
        /// <summary>
        /// 用户openid           
        /// </summary>
        public int openid { get; set; }
    }
}
