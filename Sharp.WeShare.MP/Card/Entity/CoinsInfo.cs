﻿namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 券点余额信息
    /// </summary>
    public class CoinsInfo
    {
        /// <summary>
        /// 免费券点数目
        /// </summary>
        public decimal free_coin { get; set; }
        public decimal pay_coin { get; set; }
        /// <summary>
        /// 全部券点数目
        /// </summary>
        public decimal total_coin { get; set; }
    }
}