﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 创建卡券时，响应实体
    /// </summary>
    public class CardIdRes:BaseRes
    {
        public string card_id { get; set; }
    }
}
