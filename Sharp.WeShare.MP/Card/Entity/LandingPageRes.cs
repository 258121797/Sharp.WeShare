﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 创建卡券货架返回的实体类
    /// </summary>
    public class LandingPageRes : BaseRes
    {
        /// <summary>
        /// 货架链接。
        /// </summary>
        public string url { get; set; }
        /// <summary>
        ///货架ID。货架的唯一标识。
        /// </summary>
        public int page_id { get; set; }
    }
}
