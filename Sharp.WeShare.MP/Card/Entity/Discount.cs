﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 折扣券
    /// </summary>
    public class Discount:BaseCard
    {
        public CardInfo discount { get; set; }
        public Discount()
        {
            this.card_type = "DISCOUNT";
        }
    }
}
