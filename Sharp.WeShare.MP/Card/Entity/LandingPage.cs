﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 创建卡券货架实体类
    /// </summary>
    public class LandingPage
    {
        /// <summary>
        /// 页面的banner图片链接，须调用，建议尺寸为640*300。
        /// </summary>
        public string banner { get; set; }
        /// <summary>
        /// 页面的title。
        /// </summary>
        public string page_title { get; set; }
        /// <summary>
        /// 页面是否可以分享,填入true/false
        /// </summary>
        public bool can_share { get; set; }
        /// <summary>
        /// 投放页面的场景值；
        /// </summary>
        public CardScene scene { get; set; }
        /// <summary>
        /// 卡券列表，每个item有两个字段	
        /// </summary>
        public List<CardInfo> card_list { get; set; }
        public class CardInfo
        {
            /// <summary>
            /// 所要在页面投放的card_id
            /// </summary>
            public string card_id { get; set; }
            /// <summary>
            /// 缩略图url
            /// </summary>
            public string thumb_url { get; set; }
        }
    }
    
}
