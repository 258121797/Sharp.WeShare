﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class CardColors : BaseRes
    {
        public List<Color> colors { get; set; }
    }
    public class Color
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}
