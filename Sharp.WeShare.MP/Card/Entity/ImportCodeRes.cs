﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 导入自定义code，返回的结果。
    /// </summary>
   public class ImportCodeRes:BaseRes
    {
        ///成功个数
        public string[] succ_code { get; set; }
        /// <summary>
        /// 重复导入的code会自动被过滤
        /// </summary>
        public string[] duplicate_code { get; set; }
        /// <summary>
        /// 失败个数
        /// </summary>
        public string[] fail_code { get; set; }
    }
}
