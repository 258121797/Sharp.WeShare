﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 图文列表，显示在详情内页，优惠券券开发者须至少传入一组图文列表
    /// </summary>
    public class TextImage
    {
        public string image_url { get; set; }
        /// <summary>
        /// 图文描述
        /// </summary>
        public string text { get; set; }
    }
}
