﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
  public  class CodeCount:BaseRes
    {
        /// <summary>
        /// 已经成功存入的code数目。
        /// </summary>
        public int count { get; set; }
    }
}
