﻿namespace Sharp.WeShare.MP.Card.Entity
{
    public class RechargeQr
    {
        /// <summary>
        /// 本次支付的订单号，用于查询订单状态
        /// </summary>
        public string order_id { get; set; }
        /// <summary>
        /// 支付二维码的的链接，开发者可以调用二维码生成的公开库转化为二维码显示在网页上，微信扫码支付
        /// </summary>
        public string qrcode_url { get; set; }
        /// <summary>
        /// 二维码的数据流，开发者可以使用写入一个文件的方法显示该二维码
        /// </summary>
        public string qrcode_buffer { get; set; }
    }
}