﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 团购券
    /// </summary>
    public class GeneralCoupon:BaseCard
    {
        public CardInfo generalcoupon { get; set; }
        public GeneralCoupon()
        {
            this.card_type = "GENERALCOUPON";
        }
    }
}
