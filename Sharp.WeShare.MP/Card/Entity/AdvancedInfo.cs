﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 卡券高级字段
    /// </summary>
    public class AdvancedInfo
    {
        /// <summary>
        /// 使用门槛（条件）字段，若不填写使用条件则在券面拼写：无最低消费限制，全场通用，不限品类；并在使用说明显示：可与其他优惠共享
        /// </summary>
        public UseCondition use_condition { get; set; }
        /// <summary>
        /// 封面摘要结构体名称
        /// </summary>
        public Abstract @abstract { get; set; }
        /// <summary>
        /// 图文列表，显示在详情内页，优惠券券开发者须至少传入一组图文列表
        /// </summary>
        public List<TextImage> text_image_list { get; set; }
        /// <summary>
        /// 使用时段限制
        /// </summary>
        public List<TimeLimit> time_limit { get; set; }
        /// <summary>
        /// 商家服务类型
        /// </summary>
        public List<BusinessServiceType> business_service { get; set; }
        /// <summary>
        /// 核销后送券的数量，可设置核销后送本卡券的数量，限制传入1张，与consume_share_card_list字段互斥，朋友的券专用
        /// </summary>
        public int? consume_share_self_num { get; set; }
        /// <summary>
        /// 核销后赠送其他卡券的列表	
        /// </summary>
        public List<ShareCard> consume_share_card_list { get; set; }
        /// <summary>
        /// 是否支持分享给朋友使用，填写true优惠券才可被共享，朋友的券专用
        /// </summary>
        public bool? share_friends { get; set; }
    }

    public class ShareCard
    {
        /// <summary>
        /// 核销后赠送的其他卡券card_id，目前仅支持填入一个共享券card_id，注意此处必须填入共享券
        /// </summary>
        public string card_id { get; set; }
        /// <summary>
        /// 核销后赠送的该card_id数目，目前仅支持填1
        /// </summary>
        public int num { get; set; }
       
    }
}
