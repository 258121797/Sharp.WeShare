﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 卡券基类
    /// </summary>
    public abstract class BaseCard
    {
        public string card_type { get; set; }
        /// <summary>
        /// 卡券ID，更新卡券信息是专用
        /// </summary>
        public string card_id { get; set; }
    }
}
