﻿namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    ///生成二维码时，领取多张卡券的参数
    /// </summary>
    public class MultipleCardParam
    {
        /// <summary>
        /// 卡券ID
        /// </summary>
        public string card_id { get; set; }
        /// <summary>
        /// 卡券Code码,use_custom_code字段为true的卡券必须填写，非自定义code和导入code模式的卡券不必填写。
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// 对于会员卡的二维码，用户每次扫码打开会员卡后点击任何url，会将该值拼入url中，方便开发者定位扫码来源
        /// </summary>
        public string outer_str { get; set; } 
    }
}