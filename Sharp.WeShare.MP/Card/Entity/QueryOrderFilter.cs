﻿using System;
using Sharp.WeShare.MP.EnumKey;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class QueryOrderFilter
    {
        /// <summary>
        /// 分批查询的起点，默认为0
        /// </summary>
        public int offset { get; set; }
        /// <summary>
        /// 分批查询的数量
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// 批量查询订单的起始事件，默认1周前
        /// </summary>
        public DateTime begin_time { get; set; }
        /// <summary>
        /// 批量查询订单的结束事件,默认为当前时间
        /// </summary>
        public DateTime end_time { get; set; }
        /// <summary>
        /// 所要拉取的订单类型
        /// </summary>
        public CoinOrderType order_type { get; set; }
        /// <summary>
        /// 反选，不要拉取的订单状态
        /// </summary>
        public PayCoinStatus NorStatus { get; set; }
        /// <summary>
        /// 排序依据，SORT_BY_TIME 以订单时间排序
        /// </summary>
        public string sort_key { get; set; }
        /// <summary>
        /// 排序规则，SORT_ASC 升序SORT_DESC 降序
        /// </summary>
        public SortType sort_type { get; set; }

        public QueryOrderFilter()
        {
            this.sort_key = "SORT_BY_TIME";
        }
    }
}