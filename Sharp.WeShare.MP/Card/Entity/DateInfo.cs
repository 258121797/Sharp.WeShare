﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class DateInfo
    {
        /// <summary>
        /// 使用时间的类型，仅支持选择一种时间类型的字段填入。朋友的券时，仅支持DATE_TYPE_FIX_TIME_RANGE
        /// </summary>
        public CardDateType type { get; set; }
        /// <summary>
        /// type为DATE_TYPE_FIX_TIME_RANGE时专用，表示起用时间。格式为时间戳
        /// </summary>
        public int begin_timestamp { get; set; }
        /// <summary>
        ///  表示结束时间，建议设置为截止日期的23:59:59过期。格式为时间戳。当用于DATE_TYPE_FIX_TERM时，表示卡券统一过期时间，建议设置为截止日期的23:59:59过期。设置了fixed_term卡券，当时间达到end_timestamp时卡券统一过期
        /// </summary>
        public int end_timestamp { get; set; }
        /// <summary>
        /// type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天内有效，不支持填写0。
        /// </summary>
        public int? fixed_term { get; set; }
        /// <summary>
        /// type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天开始生效，领取后当天生效填写0。（单位为天）
        /// </summary>
        public int? fixed_begin_term { get; set; }
    }
}
