﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class CardInfoRes : BaseRes
    {
        /// <summary>
        /// 卡券信息
        /// </summary>
        public BaseCard card { get; set; }
    }
}
