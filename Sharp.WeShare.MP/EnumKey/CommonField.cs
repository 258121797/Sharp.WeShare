﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 微信格式化的选项类型。
    /// </summary>
    public enum CommonField
    {
        /// <summary>
        /// 手机号
        /// </summary>
        USER_FORM_INFO_FLAG_MOBILE,
        /// <summary>
        /// 性别
        /// </summary>
        USER_FORM_INFO_FLAG_SEX,
        /// <summary>
        /// 姓名
        /// </summary>
        USER_FORM_INFO_FLAG_NAME,
        /// <summary>
        /// 生日
        /// </summary>
        USER_FORM_INFO_FLAG_BIRTHDAY,
        /// <summary>
        /// 身份证
        /// </summary>
        USER_FORM_INFO_FLAG_IDCARD,
        /// <summary>
        /// 邮箱
        /// </summary>
        USER_FORM_INFO_FLAG_EMAIL,
        /// <summary>
        /// 详细地址
        /// </summary>
        USER_FORM_INFO_FLAG_LOCATION,
        /// <summary>
        /// 教育背景
        /// </summary>
        USER_FORM_INFO_FLAG_EDUCATION_BACKGRO,
        /// <summary>
        /// 职业
        /// </summary>
        USER_FORM_INFO_FLAG_CAREER,
        /// <summary>
        /// 行业
        /// </summary>
        USER_FORM_INFO_FLAG_INDUSTRY,
        /// <summary>
        /// 收入
        /// </summary>
        USER_FORM_INFO_FLAG_INCOME,
        /// <summary>
        /// 兴趣爱好
        /// </summary>
        USER_FORM_INFO_FLAG_HABIT
    }
}
