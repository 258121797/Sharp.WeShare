﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    public enum CodeCardStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        NORMAL,
        /// <summary>
        /// 已核销
        /// </summary>
        CONSUMED,
        /// <summary>
        /// 已过期
        /// </summary>
        EXPIRE,
        /// <summary>
        /// 转赠中
        /// </summary>
        GIFTING,
        /// <summary>
        /// 转赠超时
        /// </summary>
        GIFT_TIMEOUT,
        /// <summary>
        /// 已删除
        /// </summary>
        DELETE,
        /// <summary>
        /// 已失效
        /// </summary>
        UNAVAILABLE
    }
}
