﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    public enum CodeType
    {
        /// <summary>
        /// 文本
        /// </summary>
        CODE_TYPE_TEXT,
        /// <summary>
        /// 一维码 
        /// </summary>
        CODE_TYPE_BARCODE,
        /// <summary>
        /// 二维码
        /// </summary>
        CODE_TYPE_QRCODE,
        /// <summary>
        /// 二维码无code显示
        /// </summary>
        CODE_TYPE_ONLY_QRCODE,
        /// 一维码无code显示； 
        CODE_TYPE_ONLY_BARCODE,
        /// <summary>
        /// 不显示code和条形码类型
        /// </summary>
        CODE_TYPE_NONE

    }
}
