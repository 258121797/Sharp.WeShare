﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    public enum CardDateType
    {
        /// <summary>
        /// 表示固定日期区间
        /// </summary>
        DATE_TYPE_FIX_TIME_RANGE,
        /// <summary>
        /// 表示固定时长.自领取后按天算。
        /// </summary>
        DATE_TYPE_FIX_TERM
    }
}
