﻿namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 券点订单类型
    /// </summary>
    public enum CoinOrderType
    {
        /// <summary>
        /// 平台赠送
        /// </summary>
        ORDER_TYPE_SYS_ADD,
        /// <summary>
        /// 充值
        /// </summary>
        ORDER_TYPE_WXPAY,
        /// <summary>
        /// 库存回退券点
        /// </summary>
        ORDER_TYPE_REFUND,
        /// <summary>
        /// 券点兑换库存
        /// </summary>
        ORDER_TYPE_REDUCE,
        /// <summary>
        /// 平台扣减
        /// </summary>
        ORDER_TYPE_SYS_REDUCE
    }
}