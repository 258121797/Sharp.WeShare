﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 邀请状态
    /// </summary>
    public enum InviteStatus
    {
        /// <summary>
        /// 等待确认
        /// </summary>
        waiting,
       /// <summary>
       /// 被拒绝
       /// </summary>
        rejected,
        /// <summary>
        /// 过期
        /// </summary>
        expired
    }
}
