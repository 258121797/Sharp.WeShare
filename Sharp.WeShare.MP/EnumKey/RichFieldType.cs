﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 富文本类型                
    /// </summary>
    public enum RichFieldType
    {
        /// <summary>
        /// 自定义单选
        /// </summary>
        FORM_FIELD_RADIO,
        /// <summary>
        /// 自定义选择项
        /// </summary>
        FORM_FIELD_SELECT,
        /// <summary>
        /// 自定义多选
        /// </summary>
        FORM_FIELD_CHECK_BOX
    }
}
