﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 商家服务类型枚举
    /// </summary>
    public enum BusinessServiceType
    {/// <summary>
     /// 外卖服务
     /// </summary>
        BIZ_SERVICE_DELIVER,
        /// <summary>
        /// 停车位
        /// </summary>
        BIZ_SERVICE_FREE_PARK,
        /// <summary>
        ///  可带宠物
        /// </summary>
        BIZ_SERVICE_WITH_PET,
        /// <summary>
        /// 免费wifi
        /// </summary>
        BIZ_SERVICE_FREE_WIFI
    }
}
