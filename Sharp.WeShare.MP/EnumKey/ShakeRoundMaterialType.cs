﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 要周边素材类型
    /// </summary>
    public enum ShakeRoundMaterialType
    {
        /// <summary>
        /// 摇一摇页面展示的icon图
        /// </summary>
        icon,
        /// <summary>
        /// 申请开通摇一摇周边功能时需上传的资质文件
        /// </summary>
        license
    }
}
