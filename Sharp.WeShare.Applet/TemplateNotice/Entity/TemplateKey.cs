﻿namespace Sharp.WeShare.Applet.TemplateNotice.Entity
{
    /// <summary>
    /// 模板消息字段实体
    /// </summary>
    public class TemplateKey
    {
        public string value { get; set; }
        public string color { get; set; }
    }
}
