﻿namespace Sharp.WeShare.Applet.Login.Entity
{
    public class MySession:BaseRes
    {
        /// <summary>
        /// 真实的session_key
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 加密后的session_key
        /// </summary>
        public string MyKey { get; set; }

        public string OpenId { get; set; }
        /// <summary>
        /// session_key的的有效时间
        /// </summary>
        public int ExpiresIn { get; set; }
    }
}