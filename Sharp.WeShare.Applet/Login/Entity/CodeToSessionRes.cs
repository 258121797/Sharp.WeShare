﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Applet.Login.Entity
{
    /// <summary>
    /// code换取session
    /// </summary>
    public class CodeToSessionRes : BaseRes
    {
        public string session_key { get; set; }
        public int expires_in { get; set; }
        public string openid { get; set; }
    }
}
