﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Sharp.WeShare.Applet.Login.Entity
{
   public class UserInfo
    {
        [JsonProperty("openId")]
        public string OpenId { get; set; }
        [JsonProperty("nickName")]
        public string NickName { get; set; }
        [JsonProperty("gender")]
        public int Gender { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("province")]
        public string Province { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("avatarUrl")]
        public string AvatarUrl { get; set; }
        [JsonProperty("unionId")]
        public string UnionId { get; set; }
        [JsonProperty("watermark")]
        public Watermark Watermark { get; set; }
    }
    public class Watermark
    {
        [JsonProperty("appid")]
        public string Appid { get; set; }
        [JsonProperty("timestamp")]
        public int Timestamp { get; set; }
    }

}
